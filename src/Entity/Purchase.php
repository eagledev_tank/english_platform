<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\PurchaseRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PurchaseRepository::class)]
#[ApiResource]
class Purchase
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $payedAmount = null;

    #[ORM\Column]
    private ?bool $isPayed = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayedAmount(): ?int
    {
        return $this->payedAmount;
    }

    public function setPayedAmount(int $payedAmount): self
    {
        $this->payedAmount = $payedAmount;

        return $this;
    }

    public function isIsPayed(): ?bool
    {
        return $this->isPayed;
    }

    public function setIsPayed(bool $isPayed): self
    {
        $this->isPayed = $isPayed;

        return $this;
    }
}
